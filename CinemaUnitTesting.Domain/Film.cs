﻿using CinemaUnitTesting.Domain.Interfaces;

namespace CinemaUnitTesting.Domain
{
    public class Film : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Certificate Certificate { get; set; }
    }
}