﻿using CinemaUnitTesting.Services;
using CinemaUnitTesting.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CinemaUnitTesting.Tests
{
    [TestClass]
    public class AgeVerificationServiceTests
    {
        [TestMethod]
        public void AgeCannotBeNegative()
        {
            AgeVerificationService ageVerificationService =
                new AgeVerificationService();

            bool isValid = ageVerificationService.IsValidAge(-1);

            Assert.IsFalse(isValid);
        }
    }
}