﻿using System.Collections.Generic;
using CinemaUnitTesting.Domain;

namespace CinemaUnitTesting.Services.Interfaces
{
    public interface IFilmRetrievalService
    {
        IEnumerable<Film> GetFilms(string certificateName);
        Film GetFilm(int id);
        bool ShouldCheckViewerAge(Film film);
    }
}