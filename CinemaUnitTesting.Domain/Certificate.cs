﻿using CinemaUnitTesting.Domain.Interfaces;

namespace CinemaUnitTesting.Domain
{
    public class Certificate : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? MinAge { get; set; }
    }
}