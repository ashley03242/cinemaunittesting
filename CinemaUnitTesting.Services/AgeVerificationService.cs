﻿using CinemaUnitTesting.Domain;
using CinemaUnitTesting.Services.Interfaces;

namespace CinemaUnitTesting.Services
{
    public class AgeVerificationService : IAgeVerificationService
    {
        public bool IsValidAge(int age)
        {
            return age > 0;
        }

        public bool CanViewFilm(Film film, int age)
        {
            return film.Certificate.MinAge == null || film.Certificate.MinAge >= age;
        }
    }
}