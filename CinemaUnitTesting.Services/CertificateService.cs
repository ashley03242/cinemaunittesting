﻿using System.Collections.Generic;
using CinemaUnitTesting.Data.Interfaces;
using CinemaUnitTesting.Domain;
using CinemaUnitTesting.Services.Interfaces;

namespace CinemaUnitTesting.Services
{
    public class CertificateService : ICertificateService
    {
        private readonly IRepository _repository;

        public CertificateService(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Certificate> GetCertificates()
        {
            return _repository.GetCertificates();
        }
    }
}