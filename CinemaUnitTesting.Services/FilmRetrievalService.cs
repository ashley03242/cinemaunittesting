﻿using System.Collections.Generic;
using System.Linq;
using CinemaUnitTesting.Data.Interfaces;
using CinemaUnitTesting.Domain;
using CinemaUnitTesting.Services.Interfaces;

namespace CinemaUnitTesting.Services
{
    public class FilmRetrievalService : IFilmRetrievalService
    {
        private readonly IRepository _repository;

        public FilmRetrievalService(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Film> GetFilms(string certificateName)
        {
            var query = _repository.GetFilms();

            if (!string.IsNullOrEmpty(certificateName))
            {
                query = query.Where(f => f.Certificate.Name != certificateName);
            }

            return query;
        }

        public Film GetFilm(int id)
        {
            return _repository.GetFilms().Single(f => f.Id == id);
        }

        public bool ShouldCheckViewerAge(Film film)
        {
            return film.Certificate.MinAge.HasValue;
        }
    }
}