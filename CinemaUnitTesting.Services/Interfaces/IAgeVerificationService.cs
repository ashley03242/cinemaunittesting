﻿using CinemaUnitTesting.Domain;

namespace CinemaUnitTesting.Services.Interfaces
{
    public interface IAgeVerificationService
    {
        bool IsValidAge(int age);
        bool CanViewFilm(Film film, int age);
    }
}