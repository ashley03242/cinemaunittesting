﻿using System.Collections.Generic;
using CinemaUnitTesting.Data.Interfaces;
using CinemaUnitTesting.Domain;
using CinemaUnitTesting.Services;
using CinemaUnitTesting.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CinemaUnitTesting.Tests
{
    [TestClass]
    public class FilmRetrievalServiceTests
    {
        private IFilmRetrievalService _filmRetrievalService;
        private Mock<IRepository> _mockRepository;

        [TestInitialize]
        public void Setup()
        {
            _mockRepository = new Mock<IRepository>();
            _filmRetrievalService = new FilmRetrievalService(_mockRepository.Object);

            SetupFilmList();
        }

        public void SetupFilmList()
        {
            _mockRepository.Setup(r => r.GetFilms()).Returns(new List<Film>
            {
                new Film { Id = 1, Name = "Snow White", Certificate = new Certificate { Id = 2, Name = "U" } }
            });
        }

        [TestMethod]
        public void RetrievesCorrectFilm()
        {
            var film = _filmRetrievalService.GetFilm(1);
            Assert.AreEqual("Snow White", film.Name);
        }
    }
}