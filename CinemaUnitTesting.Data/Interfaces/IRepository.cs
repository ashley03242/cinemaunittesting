﻿using System.Collections.Generic;
using CinemaUnitTesting.Domain;

namespace CinemaUnitTesting.Data.Interfaces
{
    public interface IRepository
    {
        IEnumerable<Film> GetFilms();
        IEnumerable<Certificate> GetCertificates();
    }
}