﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using CinemaUnitTesting.Data;
using CinemaUnitTesting.Data.Interfaces;
using CinemaUnitTesting.Domain;
using CinemaUnitTesting.Services;
using CinemaUnitTesting.Services.Interfaces;

namespace CinemaUnitTesting
{
    public class Program
    {
        private static IFilmRetrievalService _filmRetrievalService;
        private static ICertificateService _certificateService;
        private static IAgeVerificationService _ageVerificationService;

        private static void Main(string[] args)
        {
            GetServices();

            var requiredCertificate = GetRequiredCertificate();
            var films = _filmRetrievalService.GetFilms(requiredCertificate);

            WriteFilmList(films);

            var film = GetDesiredFilm();

            CheckCertificate(film);

            Console.ReadLine();
        }

        private static string GetRequiredCertificate()
        {
            var certificates = _certificateService.GetCertificates().ToList();

            string inputtedString;

            do
            {
                Console.Write("Enter desired certificate, or leave empty to see all films: ");
                inputtedString = Console.ReadLine()?.ToLower().Trim();
            } while (!string.IsNullOrEmpty(inputtedString) && certificates.All(c => c.Name.ToLower() != inputtedString));

            Console.WriteLine();

            return inputtedString;
        }

        private static void WriteFilmList(IEnumerable<Film> films)
        {
            Console.WriteLine("The following films are available: ");
            Console.WriteLine();

            foreach (var film in films)
            {
                Console.WriteLine($"{film.Id} - {film.Name} ({film.Certificate.Name})");
            }

            Console.WriteLine();
        }

        private static Film GetDesiredFilm()
        {
            Console.Write("Enter desired film number: ");

            var inputtedString = Console.ReadLine();

            var inputtedInt = int.Parse(inputtedString);
            var film = _filmRetrievalService.GetFilm(inputtedInt);

            return film;
        }

        private static void CheckCertificate(Film film)
        {
            const string successMessage = "Enjoy the film!";

            if (!_filmRetrievalService.ShouldCheckViewerAge(film))
            {
                Console.WriteLine(successMessage);
                return;
            }

            Console.WriteLine();

            while (true)
            {
                Console.Write("Enter your age: ");

                try
                {
                    var ageString = Console.ReadLine();
                    var ageInt = int.Parse(ageString);

                    if (!_ageVerificationService.IsValidAge(ageInt))
                    {
                        throw new Exception();
                    }

                    Console.WriteLine(_ageVerificationService.CanViewFilm(film, ageInt)
                        ? successMessage
                        : "Sorry, you are too young to see this film.");

                    return;
                }
                catch
                {
                    Console.WriteLine("Please enter a valid age.");
                }
            }
        }

        private static void GetServices()
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<IRepository, Repository>()
                .AddScoped<ICertificateService, CertificateService>()
                .AddScoped<IAgeVerificationService, AgeVerificationService>()
                .AddScoped<IFilmRetrievalService, FilmRetrievalService>()
                .BuildServiceProvider();

            _filmRetrievalService = serviceProvider.GetService<IFilmRetrievalService>();
            _certificateService = serviceProvider.GetService<ICertificateService>();
            _ageVerificationService = serviceProvider.GetService<IAgeVerificationService>();
        }
    }
}