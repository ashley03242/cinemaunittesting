﻿using System.Collections.Generic;
using CinemaUnitTesting.Domain;

namespace CinemaUnitTesting.Services.Interfaces
{
    public interface ICertificateService
    {
        IEnumerable<Certificate> GetCertificates();
    }
}