﻿using System.Collections.Generic;
using System.Linq;
using CinemaUnitTesting.Data.Interfaces;
using CinemaUnitTesting.Domain;

namespace CinemaUnitTesting.Data
{
    public class Repository : IRepository
    {
        private readonly IEnumerable<Certificate> _certificates;
        private readonly IEnumerable<Film> _films;

        public Repository()
        {
            _certificates = new List<Certificate>
            {
                new Certificate { Id = 1, Name = "U" },
                new Certificate { Id = 2, Name = "PG" },
                new Certificate { Id = 3, Name = "12A", MinAge = 12 },
                new Certificate { Id = 4, Name = "15", MinAge = 15 },
                new Certificate { Id = 5, Name = "18", MinAge = 18 }
            };

            _films = new List<Film>
            {
                new Film { Id = 1, Name = "The Shawshank Redemption", Certificate = _certificates.Single(c => c.Id == 4) },
                new Film { Id = 2, Name = "Frozen", Certificate = _certificates.Single(c => c.Id == 2) },
                new Film { Id = 3, Name = "Jaws", Certificate = _certificates.Single(c => c.Id == 3) },
                new Film { Id = 4, Name = "A Clockwork Orange", Certificate = _certificates.Single(c => c.Id == 5) },
                new Film { Id = 5, Name = "Toy Story 4", Certificate = _certificates.Single(c => c.Id == 1) },
                new Film { Id = 6, Name = "On Her Majesty's Secret Service", Certificate = _certificates.Single(c => c.Id == 2) }
            };
        }

        public IEnumerable<Film> GetFilms()
        {
            return _films;
        }

        public IEnumerable<Certificate> GetCertificates()
        {
            return _certificates;
        }
    }
}